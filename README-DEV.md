# Getting Set Up With Oqtant

## Set up Poetry

Run the following command to install poetry on your system. Replace `<python>` with whichever alias is pointed towards a python binary that is >=3.10. Be sure to run this command under the user you will be developing with.

```bash
curl -sSL https://install.python-poetry.org | <python> -
```

Poetry will install the `poetry` command in it's bin directory located at `$HOME/.local/bin` of the user who ran the install command. In order to run poetry using this command you will need to ensure this location is in your PATH. You can add it to your PATH by running the following command or adding it to your shell of choice's configuration file (.bashrc, zshrc)

```
export PATH="$HOME/.local/bin:$PATH"
```

Verify the install was successful with the following command:

```
poetry --version
```

Once installed and verified update the following configuration settings:

```bash
poetry config virtualenvs.create false
poetry config virtualenvs.in-project true
```

## Create GitLab Personal Access Token

A personal GitLab access token will be required to use this package. Follow the steps here: https://gitlab.com/-/profile/personal_access_tokens

When you create a personal access token you will only have access to it in GitLab when its created, it is recommended you store this token in a safe place as you will need to use it later.

## Setup Repo

This project is hosted on our GitLab registry, test-pypi, and pypi. In order for poetry to be able to publish to each of these some configuration is required.

### Gitlab Registry

In order to publish this package to our Gitlab registry you will need to configure the Bert Packages repository in your poetry config.

```bash
poetry config repositories.bert-packages https://gitlab.com/api/v4/projects/39544653/packages/pypi
poetry config pypi-token.bert-packages <personal access token>
poetry config http-basic.bert-packages __token__ <personal access token>
```

### Test Pypi

In order to publish this package to test-pypi you will need to have account on test-pypi that has access to https://test.pypi.org/project/oqtant/.

Once you have an account set up with access to the project in test-pypi you will need to generate an API token. To do this navigate to your account settings and where it says "API Tokens" select the option to add one and follow the steps. Once you have a token you can run the following:

```bash
poetry config repositories.test-pypi https://test.pypi.org/legacy/
poetry config pypi-token.test-pypi <test-pypi token>
```

### Pypi

In order to publish this package to pypi you will need to have account on pypi that has access to https://pypi.org/project/oqtant/.

Once you have an account set up with access to the project in pypi you will need to generate an API token. To do this navigate to your account settings and where it says "API Tokens" select the option to add one and follow the steps. Once you have a token you can run the following:

```bash
poetry config pypi-token.pypi <pypi token>
```

### Create Virtual Environment

A virtual environment will need to be created so that the required python packages can be installed for the project. To create one run the following command and replace `<python>` with whichever alias is pointed towards a python binary that is >=3.10:

```
<python> -m venv .venv
```

Once created you will need to activate it. Activating the virtual environment will be required going forward to run the project. To activate run the following:

```
source .venv/bin/activate
```

_NOTE_: Best practice is to create the virtual environment in the root of the project so in the future its easily located

### Install Requirements

Install the requirements specified `oqtant/pyproject.toml`.

```
poetry install
```

### Setup Pre-Commit

This project uses pre-commit for maintaining code uniformity and quality. Once you have the requirements installed from poetry run the following command to get pre-commit configured:

```
pre-commit install
pre-commit install --hook-type pre-push
```

This command will cause our selected checks to run on every git commit and push.

## Using the Client

Oqtant is primarily used within a Jupyter Notebook. Within this project are a set of walkthrough and demo notebooks that can be used to test and develop with.

If you wish to use the client outside of the notebooks you can do so via a python shell.

### Using Jupyter Notebooks

Within the root of this project run the following command:

```
jupyter notebook
```

The above command will open a new tab in your web browser with the Jupyter Notebook UI. Within this UI you should see a directory called `documentation`. Within this directory you will find the walkthrough and demo directories which contain notebooks that you can use. Each notebook includes instructions on how to use it.

### Using Python Shell

Should you wish to use the client outside of a notebook you can do so with a python terminal. To do so follow the below:

```python
>>> from oqtant.util.auth import get_user_token
>>> from oqtant.oqtant_client import get_oqtant_client
>>> token = get_user_token()
>>> client = get_oqtant_client(token=token)
>>> client
<oqtant.oqtant_client.OqtantClient object at 0x7f6f8611acb0>
>>>
```

One important thing to note is that you will need to follow the same authentication flow within the notebooks in order for things to work.

## Development Specific Notes

When working on the Oqtant client it is important to be able to test things e2e. By default this project is pointed to our development Oqtant REST API environment. If you would like to run it against your locally running bert_rest_service you can tweak the following:

```
base_url=http://localhost:5000/api/v1/bert/jobs
```

## Running Tests

Oqtant Client tests can be run using pytest in the command line or with other tools should you prefer. 


## Building

To build Oqtant run the following:

```bash
poetry build
```

## Publishing

When you are ready to publish a new version of this package run the following:

### Gitlab registry

```bash
poetry publish -r bert-packages
```

### Test Pypi

```bash
poetry publish -r test-pypi
```

### Pypi

```bash
poetry publish
```

## Installing from Pypi

There are times where it is helpful to install and run the distributable version of the code. To do this simply run the following:

pip install oqtant

### Installing from whl

During development it is important to ensure the code runs as intended after being bundled into a whl file. You can test things out by installing a new version of the code via whl with the following command:

pip install <path-to-new-whl-file>.whl

## Changelog process

We use an automated changelog process within the Gitlab repository. There are two aspects of this process. First is the changelog verb in the squash commit and the second is the tagging process.

### Requirements

You need to have an approved, but not yet merged, pull request with your changes from a feature branch.

### Gitlab UI Steps

1. Click on the “Edit commit message” checkbox inside the merge request

   ![Example for Edit Commit Message](/misc/merge1-example.png)

2. Update the Squash Commit message by adding a useful message

   ![Example for Squish Commit Message](/misc/merge2-example.png)

3. Update the "Changelog: added|removed|updated|fixed" section by deleting all other changelog options other than what your change produced.

   Example:If your change is adding functionality you should update this section to be "Changelog: added"
   ![Final Merge Example](/misc/merge3-example.png)

4. Click Merge

### CMD line git Steps

For changes that are done directly to develop from local, or manual squash merges, you add `--trailer "Changelog: <verb>"` to the end of the commit command

### Tagging process

Now that the changes are in we can generate the CHANGELOG.md for the first time. Navigate to the tags section of your repository and create a tag (we use semantic versioning so each new tag should be at least one fix version above the previous). After creating the tag you can navigate to the CI/CD section of your repository and see a new pipeline running titled “changelog”. Monitor it until finished (successfully) and now check the default branch in your repository, you should see a new commit from your Project Access Token that contains the new CHANGELOG.md!

### Useful links

[Link to Automatic Changelog Demo](https://coldquanta.atlassian.net/l/cp/cfx5d1oq)

[Link to Gitlab Changelog Entries](https://docs.gitlab.com/ee/development/changelog.html)
