<!-- markdownlint-disable -->

<a href="../../oqtant/start_notebook.py#L0"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

# <kbd>module</kbd> `start_notebook`





---

<a href="../../oqtant/start_notebook.py#L8"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `get_running_ports`

```python
get_running_ports()
```

Get running ports of Jupyter servers 



**Args:**
  None 

**Returns:**
 
 - <b>`list`</b>:  list of running ports 




---

_This file was automatically generated via [lazydocs](https://github.com/ml-tooling/lazydocs)._
