<!-- markdownlint-disable -->

<a href="../../oqtant/simulator/three_dim.py#L0"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

# <kbd>module</kbd> `simulator.three_dim`






---

<a href="../../oqtant/simulator/three_dim.py#L20"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>class</kbd> `ThreeDimGrid`
'ThreeDimGrid' Defines a two dimensional grid space in cylindrical coordinates with axial symmetry. Nx and Nr are the number of points in the x and r directions Lx and Lr are the lenghts of the x and z dimensions tof_nf toggles the near field grids tof_ff toggles the far field grids 

<a href="../../oqtant/simulator/three_dim.py#L38"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `__init__`

```python
__init__(tof_nf: bool = False, tof_ff: bool = False)
```











---

_This file was automatically generated via [lazydocs](https://github.com/ml-tooling/lazydocs)._
