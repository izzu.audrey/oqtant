# Oqtant API (OqtAPI)

Oqtant offers a Python package that works with Jupyter notebooks to provide access to more complex experiments, fine-tuned control over job inputs, and more in-depth data analysis tooling.

With [Oqtant Quantum Matter Service](https://oqtant.infleqtion.com), create a form of quantum matter called a Bose-Einstein condensate (BEC), wherein quantum phenomena are visible on a macroscopic scale. Using light, programmatically manipulate BECs to explore a multitude of phenomena: superposition, interference, tunneling, superfluidity, and more!

[![License: Apache](https://img.shields.io/badge/License-Apache-yellow.svg)](https://opensource.org/licenses/Apache-2.0)
[![pypi](https://img.shields.io/pypi/v/oqtant.svg)](https://pypi.python.org/pypi/oqtant)
[![versions](https://img.shields.io/pypi/pyversions/bert-schemas.svg)](https://pypi.python.org/pypi/bert-schemas)
[![Twitter](https://img.shields.io/twitter/url/https/twitter.com/Infleqtion.svg?style=social&label=Follow%20%40Infleqtion)](https://twitter.com/Infleqtion)

## 🚀 Installation

The easiest way to install Oqtant's Python API is via pip:

```shell
pip install oqtant
```

### Update

From a terminal, run:

```shell
pip install --upgrade oqtant
```

Or from within a Jupyter notebook, run:

```shell
%pip install --upgrade oqtant
```

To submit jobs, you will need an Oqtant account. [Register and start for free!](https://oqtant.infleqtion.com)
For more details, see the [Quick Start Guide](https://oqtant-docs.infleqtion.com/INSTALL)

## 🧭 Introduction

After installing Oqtant's Python API, you can explore the [Example Jupyter Notebooks](https://oqtant-docs.infleqtion.com/examples/hello_world/). These notebooks will walk you through the functionality and capabilities of using the Oqtant API. Additionally, there is a growing library of [Demonstration Jupyter Notebooks](https://oqtant-docs.infleqtion.com/examples/demos/demo_1_quantum_interference/) for showing specific quantum phenomena on Oqtant (e.g. interference).

Oqtant's Python API contains tools to:

- Have the programmatic equivalent of the same job functionality as the [Oqtant Web App](https://oqtant.infleqtion.com), and more

- Build parameterized (i.e. optimization) experiments using OqtantJobs

- Submit and retrieve OqtantJob results

- Analyze OqtantJob results

## 📓 Documentation

- [Quick Start Guide](https://oqtant-docs.infleqtion.com/INSTALL)
- [Oqtant API docs](https://oqtant-docs.infleqtion.com/oqtant_client_docs)
- [Oqtant REST API docs](https://oqtant-docs.infleqtion.com/oqtant_rest_api_docs)
- [Example Notebooks](https://oqtant-docs.infleqtion.com/examples/hello_world)
