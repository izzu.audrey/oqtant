# pylint: disable=redefined-outer-name
import pytest
from bert_schemas.testing.fixtures.job import *  # type: ignore
from pydantic_settings import BaseSettings
from requests.exceptions import RequestException

from oqtant.oqtant_client import OqtantClient
from oqtant.schemas.quantum_matter import QuantumMatterFactory

qmf = QuantumMatterFactory()


@pytest.fixture
def settings():
    class Settings(BaseSettings):
        auth0_domain: str = "auth0_domain"
        auth0_client_id: str = "auth0_client_id"
        auth0_scope: str = "aut0_scope"
        auth0_audience: str = "auth0_audience"
        auth0_notebook_redirect_uri: str = "auth0_notebook_redirect_uri"
        auth0_response_type: str = "auth0_response_type"
        base_url: str = "base_url"
        max_ind_var: int = 2
        run_list_limit: int = 30

    yield Settings()


@pytest.fixture
def client(settings):
    yield OqtantClient(settings=settings, token=False)


@pytest.fixture
def mock_auth0_external_id():
    yield "auth0|12345678901234567890abcd"


@pytest.fixture
def mock_external_id():
    yield "a8d55e0c-4947-46a9-a0e3-6f6fc1924e1f"


@pytest.fixture
def mock_job_create(mock_external_id):
    yield {"job_id": mock_external_id, "queue_position": 1, "est_time": 60}


@pytest.fixture
def mock_response():
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

        def raise_for_status(self):
            if self.status_code >= 400:
                raise RequestException
            return self.status_code

    yield MockResponse


@pytest.fixture
def mock_barriers():
    barrier1 = qmf.create_barrier(
        positions=[5, 5], heights=[0, 10], widths=[1, 1], times=[0, 1]
    )
    barrier1.evolve(duration=3, height=10)

    barrier2 = qmf.create_barrier(
        positions=[-5, -5], heights=[0, 10], widths=[1, 1], times=[0, 1]
    )
    barrier2.evolve(duration=3, height=10)
    yield [barrier1, barrier2]
