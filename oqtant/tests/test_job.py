from datetime import datetime

import numpy as np
import pytest

from oqtant.schemas.job import *


def test_truncated_name(job_fixtures):
    job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
    job.name = "This is a very long job name that needs to be tr"
    expected_result = "This is a very long job name that nee..."

    assert job.truncated_name == expected_result


# def test_formatted_time_submit(job_fixtures):
#     job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
#     job.time_submit = datetime.fromisoformat("2023-01-01 12:00:00")
#     expected_result = "01 Jan 2023, 12:00:00"
#
#     assert job.formatted_time_submit == expected_result
#


def test_input_fields(job_fixtures):
    job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
    expected_result = 1

    assert job.input_count == expected_result


def test_job_type_bec(job_fixtures):
    job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
    expected_result = job_schema.JobType.BEC

    assert job.job_type == expected_result


def test_job_type_barrier(job_fixtures):
    job = OqtantJob(**job_fixtures.post_barrier_job.model_dump())
    expected_result = job_schema.JobType.BARRIER

    assert job.job_type == expected_result
