import pytest

from oqtant.simulator import WaveFunction
from itertools import product
import numpy as np
import math


def test_wavefunction_init():
    # test that we get value exception for both NF and FF
    with pytest.raises(ValueError) as exc:
        WaveFunction(tof_nf=True, tof_ff=True)
    assert "tof_nf and tof_ff cannot both be True" in str(exc.value)

    wf_nf = WaveFunction(tof_nf=True, tof_ff=False)
    wf_ff = WaveFunction(tof_nf=False, tof_ff=True)
    wf = WaveFunction(tof_ff=False, tof_nf=False)

    assert wf_nf is not wf_ff and wf_nf is not wf
    assert wf is not wf_ff


def test_initial_psi():
    wf = WaveFunction(tof_ff=False, tof_nf=False)

    # assert negative x, r throws exception
    for x, r in product(range(-2, 0), range(-2, 0)):
        with pytest.raises(ValueError) as exc:
            psi = wf.initial_psi(sigma_x=x, sigma_r=r)
        assert "Wavefunction: sigma_x and sigma_r must be non-negative" in str(
            exc.value
        )


def test_normalize():
    wf = WaveFunction(tof_ff=False, tof_nf=False)
    # check type of psi
    with pytest.raises(TypeError) as exc:
        wf.normalize(psi=0)
    assert "psi must be a ndarray to normalize" in str(exc.value)

    # check dimension of psi
    with pytest.raises(ValueError) as exc:
        wf.normalize(psi=np.zeros(shape=3, dtype=int))
    assert "Cannot normalize psi" in str(exc.value)

    with pytest.raises(ValueError) as exc:
        wf.normalize(
            psi=np.zeros(shape=wf.three_d_grid.r.shape, dtype=wf.three_d_grid.r.dtype)
        ).size > 0
    assert "Cannot normalize a zero wave function" in str(exc.value)

    psi = wf.psi
    prob = wf.integrate_prob_distro(psi)
    normalized = wf.normalize(psi)

    assert np.isclose(normalized / psi, np.sqrt(1 / prob)).all()


def test_probability_distribution():
    wf = WaveFunction(tof_ff=False, tof_nf=False)
    b_zeros = wf.integrate_prob_distro(
        psi=np.zeros(shape=wf.three_d_grid.r.shape, dtype=wf.three_d_grid.r.dtype)
    )
    assert b_zeros == 0.0

    b_psi = wf.integrate_prob_distro(psi=wf.psi)
    assert math.isclose(b_psi, 1.0)  # default rel_tol == 1e-09 should be sufficient
