from unittest.mock import patch

import pytest

from oqtant.schemas.quantum_matter import QuantumMatterFactory
from oqtant.simulator import TimeSpan


@patch("oqtant.simulator.Simulator.run_rk4")
def test_get_ground_state(mock_run_rk4, client, mock_barriers):
    mock_run_rk4.return_value = None
    qmf = QuantumMatterFactory()
    qmf.client = client
    matter = qmf.create_quantum_matter(barriers=mock_barriers, lifetime=4)
    matter.submit(sim=True)
    matter.sim.set_ground_state()
    mock_run_rk4.assert_called_with(TimeSpan(start=-2.5, end=-0.01))


@patch("oqtant.simulator.Simulator.run_rk4")
def test_get_run_evolution(mock_run_rk4, client, mock_barriers):
    mock_run_rk4.return_value = None
    qmf = QuantumMatterFactory()
    qmf.client = client
    matter = qmf.create_quantum_matter(barriers=mock_barriers, lifetime=4)
    matter.submit(sim=True)
    matter.sim.psi_history = [1]

    # needs to run after get_groun_state so check that exception thrown
    with pytest.raises(ValueError) as exc:
        matter.sim.run_evolution()
    assert "Ground State not set" in str(exc.value)
    matter.sim.set_ground_state()
    matter.sim.run_evolution()
    mock_run_rk4.assert_called_with(
        TimeSpan(start=0, end=matter.sim.qm_potential.lifetime), stage_name="in-trap "
    )


def test_get_laplacian(client):
    qmf = QuantumMatterFactory()
    qmf.client = client
    matter = qmf.create_quantum_matter()
    matter.submit(sim=True)
    laplacian = matter.sim.get_laplacian(matter.sim.wavefunction.psi)
    assert laplacian.all()


def test_get_gpe(client):
    qmf = QuantumMatterFactory()
    qmf.client = client
    matter = qmf.create_quantum_matter()
    matter.submit(sim=True)
    matter.sim.qm_potential.update_potential(1)
    GPE = matter.sim.get_gpe(matter.sim.wavefunction.psi)
    assert GPE.all()


def test_convert_timesteps(client):
    qmf = QuantumMatterFactory()
    qmf.client = client
    matter = qmf.create_quantum_matter()
    matter.submit(sim=True)
    matter.sim.times = [0.001, 0.02, 0.03]
    matter.sim.convert_timesteps([0.02, 0.02, 0.02])
    assert matter.sim.time_step == 0.0005
