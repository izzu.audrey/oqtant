import pytest
import numpy as np
from oqtant.schemas.quantum_matter import QuantumMatterFactory
from oqtant.simulator import QMPotential


def test_update_potential(client, mock_barriers):
    qmf = QuantumMatterFactory()
    qmf.client = client
    matter = qmf.create_quantum_matter(barriers=mock_barriers, lifetime=4)
    qmp = QMPotential(matter)
    assert qmp.potential is None
    qmp.update_potential(1)
    maxV = np.nanmax(qmp.potential)
    assert qmp.potential is not None
    assert maxV >= 0

    with pytest.raises(ValueError) as exc:
        qmp.update_potential(10)
    assert "Potential update requested outside time bounds of the simulation" in str(
        exc.value
    )

    qmp.update_potential(1, clip=True)
    assert np.nanmax(qmp.potential) <= 400.0


def test_potential_to_cartesian(client, mock_barriers):
    qmf = QuantumMatterFactory()
    qmf.client = client
    matter = qmf.create_quantum_matter(barriers=mock_barriers, lifetime=4)
    qmp = QMPotential(matter)
    qmp.update_potential(1)
    assert qmp.potential is not None

    cart = qmp.potential_to_cartesian_oqt_units()
    assert cart is not None
    assert np.nanmax(cart) != np.nanmax(qmp.potential)
