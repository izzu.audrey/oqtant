import numpy as np
import pytest
from bert_schemas import job as job_schema

from oqtant.schemas.job import OqtantJob
from oqtant.schemas.optical import Barrier
from oqtant.schemas.quantum_matter import *


def test_from_input(post_bec_job):
    name = "My Quantum Matter"
    input_values = post_bec_job.inputs[0].values.model_dump()
    input_values["rf_evaporation"]["times_ms"] = [-1000.0, -2.0, 1.0]
    input_values["rf_evaporation"]["frequencies_mhz"] = [2.0, 2.0, 1.0]
    input_values["rf_evaporation"]["powers_mw"] = [2.0, 2.0, 1.0]
    barrier = job_schema.Barrier(
        times_ms=list(np.arange(0.0, 11.0, 1.0)),
        positions_um=list(np.arange(0.0, 11.0, 1.0)),
        heights_khz=list(np.arange(0.0, 11.0, 1.0)),
        widths_um=[1.0] * 11,
        interpolation="LINEAR",
        shape="GAUSSIAN",
    )
    input_values["optical_barriers"] = [barrier.model_dump()]
    input_values = job_schema.InputValues(**input_values)

    result = QuantumMatter.from_input(name, input_values)

    expected_result = QuantumMatter(
        name=name,
        lifetime=input_values.end_time_ms,
        time_of_flight=input_values.time_of_flight_ms,
        barriers=[barrier],
        rf_evap=RfEvap.from_input(input_values.rf_evaporation),
        rf_shield=RfShield.from_input(input_values.rf_evaporation),
    )
    assert result.model_dump() == expected_result.model_dump()

    mod_input = input_values.model_dump()
    mod_input["rf_evaporation"]["times_ms"] = [-1000.0, -2.0, 1.0]
    mod_input = job_schema.InputValues(**mod_input)

    result = QuantumMatter.from_input(name, mod_input)

    expected_result = QuantumMatter(
        name=name,
        lifetime=mod_input.end_time_ms,
        time_of_flight=mod_input.time_of_flight_ms,
        barriers=[barrier],
        rf_evap=RfEvap.from_input(mod_input.rf_evaporation),
        rf_shield=RfShield.from_input(mod_input.rf_evaporation),
    )
    assert result.model_dump() == expected_result.model_dump()


def test_from_oqtant_job(post_bec_job):
    oqtant_job = OqtantJob(**post_bec_job.model_dump())
    oqtant_job.inputs[0].values.rf_evaporation = job_schema.RfEvaporation(
        times_ms=[-1000.0, -2.0, 1.0],
        frequencies_mhz=[2.0, 2.0, 1.0],
        powers_mw=[2.0, 2.0, 1.0],
        interpolation="LINEAR",
    )
    input_values = oqtant_job.inputs[0].values
    result = QuantumMatter.from_oqtant_job(oqtant_job, "client")

    expected_result = QuantumMatter(
        name=oqtant_job.name,
        lifetime=input_values.end_time_ms,
        time_of_flight=input_values.time_of_flight_ms,
        rf_evap=RfEvap.from_input(input_values.rf_evaporation),
        rf_shield=RfShield.from_input(input_values.rf_evaporation),
        landscape=None,
        lasers=input_values.lasers,
    )
    assert result.name == expected_result.name
    assert result.rf_shield == expected_result.rf_shield
    assert result.rf_evap == expected_result.rf_evap


def test_corrected_rf_power():
    matter = QuantumMatter()
    frequency_mhz = 100.0
    power_mw = 10.0

    result = matter.corrected_rf_power(frequency_mhz, power_mw)

    expected_result = 0.9857040318457218

    assert result == expected_result


def test_corrected_rf_powers():
    matter = QuantumMatter()
    frequencies = [100.0, 200.0, 300.0]
    powers = [10.0, 20.0, 30.0]

    result = matter.corrected_rf_powers(frequencies, powers)

    expected_result = [0.9857040318457218, 1.4907974926487657, 2.20102406902539]

    assert result == expected_result


def test_get_magnetic_potential():
    matter = QuantumMatter()
    positions = [1.0, 2.0, 3.0]

    result = matter.get_magnetic_potential(positions=positions)

    expected_result = [0.010759371729701846, 0.043037486918807384, 0.0968343455673166]

    assert result == expected_result


def test_get_barrier_potential():
    matter = QuantumMatter()
    positions = [1.0, 2.0, 3.0]

    result = matter.get_potential(time=1, positions=positions, include_magnetic=False)

    expected_result = [0.0, 0.0, 0.0]

    assert result == expected_result


def test_get_landscape_potential():
    matter = QuantumMatter()
    positions = [1.0, 2.0, 3.0]

    landscape = Landscape.new()
    matter.landscape = landscape

    result = matter.get_potential(time=1, positions=positions, include_magnetic=False)

    expected_result = [0.0, 0.0, 0.0]

    assert result == expected_result


def test_get_potential():
    matter = QuantumMatter()
    positions = [1.0, 2.0, 3.0]
    landscape = Landscape.new()
    barrier = Barrier.new(positions=[0.0, 0.0], heights=[10.0, 10.0], widths=[5.0, 5.0])
    matter.barriers = [barrier]
    matter.landscape = landscape

    result = matter.get_potential(time=1, positions=positions)
    expected_result = [9.906448666515107, 9.378333236872606, 8.544923319766681]
    assert result == expected_result


def test_create_terminator():

    laser = QuantumMatterFactory.create_terminator(time_on=2, time_off=3)

    assert isinstance(laser, Laser)
