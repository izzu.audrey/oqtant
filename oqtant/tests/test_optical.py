import pytest
from bert_schemas import job as job_schema
from bert_schemas import projected
from oqtant.schemas.optical import Barrier, Landscape, Snapshot, Laser, Pulse


def test_schema_landscape_to_snapshot():
    js_landscape = job_schema.Landscape(
        time_ms=0,
        positions_um=[-10, 10],
        potentials_khz=[0, 0],
        spatial_interpolation="LINEAR",
    )
    landscape = Snapshot(**js_landscape.model_dump())
    assert isinstance(landscape, Snapshot)


def test_snapshot_from_input():
    js_landscape = job_schema.Landscape(
        time_ms=0,
        positions_um=[-10, 10],
        potentials_khz=[0, 0],
        spatial_interpolation="LINEAR",
    )
    landscape = Snapshot.from_input(js_landscape)
    assert isinstance(landscape, Snapshot)


def test_snapshot_get_potential():
    snapshot = Snapshot.new(
        positions=[-10, 10], potentials=[0, 0], interpolation="LINEAR"
    )

    positions = [-10.0, 0.0, 10.0]
    potentials = snapshot.get_potential(positions)

    assert len(potentials) == len(positions)


def test_landscape_from_input():
    js_landscape = job_schema.Landscape(
        time_ms=0,
        positions_um=[-10, 10],
        potentials_khz=[0, 0],
        spatial_interpolation="LINEAR",
    )
    optical_landscape = job_schema.OpticalLandscape(
        landscapes=[js_landscape], interpolation="LINEAR"
    )

    landscape = Landscape.from_input(optical_landscape)

    assert isinstance(landscape, Landscape)


def test_landscape_get_potential():
    # Create an instance of the Landscape class with desired values
    landscape = Landscape.new()

    time = 0.5
    positions = [-10.0, 0.0, 10.0]

    potentials = landscape.get_potential(time, positions)

    assert len(potentials) == len(positions)


def test_barrier_evolve():
    barrier = Barrier()
    barrier.evolve(duration=5.0, position=2.0)
    assert barrier.times_ms == [
        1.0,
        2.0,
        3.0,
        4.0,
        5.0,
        6.0,
        7.0,
        8.0,
        9.0,
        10.0,
        11.0,
        16.0,
    ]
    assert barrier.positions_um == [
        1.0,
        2.0,
        3.0,
        4.0,
        5.0,
        6.0,
        7.0,
        8.0,
        9.0,
        10.0,
        11.0,
        2.0,
    ]


def test_barrier_is_active():
    barrier = Barrier()
    assert barrier.is_active(2.0)
    assert barrier.is_active(2.5)
    assert not barrier.is_active(30.0)


def test_barrier_get_positions():
    barrier = Barrier()
    oned_list = barrier.get_positions([2, 3])
    assert oned_list == [2.0, 3.0]


def test_barrier_get_heights():
    barrier = Barrier()
    oned_list = barrier.get_heights([2, 3])
    assert oned_list == [10.0, 10.0]


def test_get_potential():
    barrier = Barrier()
    pots = barrier.get_potential(time=2, positions=[2.0, 3.0])
    assert pots == [10.003979461976675, 6.676165005086595]


def get_ideal_potential(time: float, positions: list):
    return [1.0] * len(positions)


def test_snapshot_get_ideal_potential():
    snapshot = Snapshot.new(
        positions=[-10, 10], potentials=[2.0, 3.0], interpolation="LINEAR"
    )
    potential = snapshot.get_ideal_potential(positions=[0])[0]
    assert potential == 2.5


def test_barrier_get_ideal_potential():
    barrier = Barrier.new(
        heights=[10.0, 10.0], positions=[0, 0], times=[0, 10], shape="SQUARE"
    )
    potential = barrier.get_ideal_potential(time=5, positions=[0])[0]
    assert potential == 10.0
