import numpy as np
import pytest
from datetime import datetime

from oqtant.schemas.job import *
from oqtant.schemas.output import *


def test_round_sig():
    result1 = round_sig(3.14159)
    assert result1 == 3.1

    result2 = round_sig(3.14159, sig=3)
    assert result2 == 3.14

    result3 = round_sig(-2.71828)
    assert result3 == -2.7


def test_TF_dist_2D():
    xy_mesh = (np.array([[1, 2, 3], [4, 5, 6]]), np.array([[7, 8, 9], [10, 11, 12]]))
    TFpOD = 0.5
    xc = 1.0
    yc = 2.0
    rx = 0.3
    ry = 0.4
    os = 0.1

    result = TF_dist_2D(xy_mesh, TFpOD, xc, yc, rx, ry, os)

    expected_result = np.array([0.1, 0.1, 0.1, 0.1, 0.1, 0.1])

    assert np.allclose(result, expected_result)


def test_Gaussian_dist_2D():
    xy_mesh = (np.array([[1, 2, 3], [4, 5, 6]]), np.array([[7, 8, 9], [10, 11, 12]]))
    GpOD = 0.5
    xc = 1.0
    yc = 2.0
    sigx = 0.3
    sigy = 0.4
    os = 0.1

    result = Gaussian_dist_2D(xy_mesh, GpOD, xc, yc, sigx, sigy, os)

    expected_result = np.array([0.1, 0.1, 0.1, 0.1, 0.1, 0.1])

    assert np.allclose(result, expected_result)


def test_bimodal_dist_2D():
    xy_mesh = (np.array([[0, 1, 2], [0, 1, 2]]), np.array([[0, 0, 0], [1, 1, 1]]))
    GpOD = 1.0
    sigx = 0.5
    sigy = 0.5
    TFpOD = 0.5
    rx = 0.3
    ry = 0.4
    xc = 1.0
    yc = 0.5
    os = 0.1

    result = bimodal_dist_2D(xy_mesh, GpOD, sigx, sigy, TFpOD, rx, ry, xc, yc, os)

    expected_result = np.array(
        [0.282085, 0.80653066, 0.282085, 0.282085, 0.80653066, 0.282085]
    )

    assert np.allclose(result, expected_result)


def test_truncated_name(job_fixtures):
    job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
    job.name = "This is a very long job name that needs to be tr"
    expected_result = "This is a very long job name that nee..."

    assert job.truncated_name == expected_result


def test_formatted_time_submit(job_fixtures):
    job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
    job.time_submit = datetime.fromisoformat("2023-01-01 12:00:00")
    expected_result = "01 Jan 2023, 12:00:00"

    assert job.formatted_time_submit == expected_result


def test_input_fields(job_fixtures):
    job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
    expected_result = 1

    assert job.input_count == expected_result


def test_job_type_bec(job_fixtures):
    job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
    expected_result = job_schema.JobType.BEC

    assert job.job_type == expected_result


def test_job_type_barrier(job_fixtures):
    job = OqtantJob(**job_fixtures.post_barrier_job.model_dump())
    expected_result = job_schema.JobType.BARRIER

    assert job.job_type == expected_result
