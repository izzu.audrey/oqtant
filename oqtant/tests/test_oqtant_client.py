from unittest.mock import MagicMock, mock_open, patch

import pytest
from bert_schemas import job as job_schema
from requests.exceptions import RequestException

from oqtant.oqtant_client import OqtantClient, get_oqtant_client
from oqtant.schemas.job import OqtantJob
from oqtant.schemas.quantum_matter import Landscape, QuantumMatter
from oqtant.schemas.quantum_matter import QuantumMatterFactory as qmf
from oqtant.schemas.quantum_matter import Snapshot
from oqtant.simulator import Simulator
from oqtant.util import exceptions as api_exceptions


def test_get_headers(settings):
    oqtant_client = OqtantClient(settings=settings, token="token")
    headers = oqtant_client._OqtantClient__get_headers()  # type: ignore
    assert headers == {
        "Authorization": "Bearer token",
        "X-Client-Version": oqtant_client.version,
    }


def test_convert_matter_to_job(client):
    matter = QuantumMatter(name="matter")
    barrier = qmf.create_barrier(
        positions=[0, 0], heights=[0, 0], times=[0, 3], widths=[3, 3], shape="GAUSSIAN"
    )
    barrier_matter = QuantumMatter(
        name="barrier matter",
        lifetime=10,
        barriers=[barrier],
        time_of_flight=10,
    )

    snapshot1 = Snapshot.new(
        time=1,
        positions=[-10, -5, 0, 5, 10],
        potentials=[0, 10, 20, 15, 0],
        interpolation="LINEAR",
    )
    snapshot2 = Snapshot.new(
        time=3,
        positions=[-20, -10, 0, 10, 20],
        potentials=[0, 15, 40, 10, 0],
        interpolation="LINEAR",
    )
    landscape = Landscape.new(snapshots=[snapshot1, snapshot2])
    paint_matter = QuantumMatter(
        name="paint matter",
        time_of_flight=10,
        lifetime=10,
        barriers=[barrier],
        landscape=landscape,
    )

    job1 = client.convert_matter_to_job(matter)
    job2 = client.convert_matter_to_job(barrier_matter)
    job3 = client.convert_matter_to_job(paint_matter)

    assert job1.name == "matter"
    assert job2.name == "barrier matter"
    assert job3.name == "paint matter"

    assert job1.job_type == job_schema.JobType.BEC
    assert job2.job_type == job_schema.JobType.BARRIER
    assert job3.job_type == job_schema.JobType.PAINT_1D

    assert len(job1.inputs) == 1
    assert len(job2.inputs) == 1
    assert len(job3.inputs) == 1

    assert job1.inputs[0].values.model_dump() == matter.input.model_dump()
    assert job2.inputs[0].values.model_dump() == barrier_matter.input.model_dump()
    assert job3.inputs[0].values.model_dump() == paint_matter.input.model_dump()


def test_submit_sim(client):
    barrier = qmf.create_barrier(
        positions=[0, 0], heights=[0, 0], times=[0, 3], widths=[3, 3], shape="GAUSSIAN"
    )
    matter = QuantumMatter(
        name="barrier matter",
        lifetime=10,
        barriers=[barrier],
        time_of_flight=10,
    )

    sim = client.submit_sim(matter)
    assert type(sim) == Simulator


@patch("oqtant.oqtant_client.OqtantClient.show_job_limits")
def test_instantiate_oqtant_client(mock_show_job_limits):
    oqtant_client = get_oqtant_client("token")
    assert isinstance(oqtant_client, OqtantClient)
    assert mock_show_job_limits.called


def test_generate_oqtant_job(settings, job_fixtures):
    oqtant_client = OqtantClient(settings=settings, token="token")
    jobs = [
        job_fixtures.complete_bec_job,
        job_fixtures.complete_barrier_job,
    ]
    for job in jobs:
        oqtant_job = oqtant_client.generate_oqtant_job(job=job.model_dump())
        assert isinstance(oqtant_job, OqtantJob)


def test_generate_oqtant_job_validation_error(settings):
    oqtant_client = OqtantClient(settings=settings, token="token")
    with pytest.raises(api_exceptions.OqtantJobValidationError):
        oqtant_client.generate_oqtant_job(job={"foo": "bar"})


def test_create_oqtant_job(settings):
    oqtant_client = OqtantClient(settings=settings, token="token")
    for job_type in [job_schema.JobType.BARRIER, job_schema.JobType.BEC]:
        oqtant_job = oqtant_client.create_job(name=f"{job_type} Job", job_type=job_type)
        assert isinstance(oqtant_job, OqtantJob)
        assert oqtant_job.job_type == job_type


def test_create_oqtant_job_batch(settings):
    oqtant_client = OqtantClient(settings=settings, token="token")
    for job_type in [job_schema.JobType.BARRIER, job_schema.JobType.BEC]:
        oqtant_job = oqtant_client.create_job(
            name=f"{job_type} Job", job_type=job_type, runs=2
        )
        assert isinstance(oqtant_job, OqtantJob)
        assert oqtant_job.job_type == job_type
        assert len(oqtant_job.inputs) == 2


def test_create_oqtant_job_with_dict(settings, job_fixtures):
    oqtant_client = OqtantClient(settings=settings, token="token")
    jobs = [
        job_fixtures.complete_bec_job,
        job_fixtures.complete_barrier_job,
    ]
    for job in jobs:
        oqtant_job = oqtant_client.create_job(name=job.name, job_type=job.job_type)
        assert isinstance(oqtant_job, OqtantJob)
        assert oqtant_job.job_type == job.job_type
        assert oqtant_job.name == job.name


def test_create_oqtant_job_bad_types(settings):
    oqtant_client = OqtantClient(settings=settings, token="token")
    for job_type in [job_schema.JobType.BRAGG, "¯\\_(ツ)_//¯"]:
        with pytest.raises(api_exceptions.OqtantJobUnsupportedTypeError):
            oqtant_client.create_job(name=f"{job_type} Job", job_type=job_type)


def test_create_oqtant_job_validation_error(settings):
    oqtant_client = OqtantClient(settings=settings, token="token")
    with pytest.raises(api_exceptions.OqtantJobValidationError):
        oqtant_client.create_job(name="foo", job_type="bar", job={"foo": "bar"})


@patch("oqtant.oqtant_client.requests.get")
def test_get_job(mock_get, settings, mock_response, mock_external_id, job_fixtures):
    oqtant_client = OqtantClient(settings=settings, token="token")
    jobs = [
        job_fixtures.pending_bec_job,
        job_fixtures.pending_bec_batch_job,
    ]

    for job in jobs:
        job_payload = job.model_dump()
        job_payload["external_id"] = mock_external_id
        mock_get.return_value = mock_response(job_payload, 200)
        oqtant_job = oqtant_client.get_job(job_id=mock_external_id)
        mock_get.assert_called_with(
            url=f"{oqtant_client.base_url}/{job_payload['external_id']}",
            headers={
                "Authorization": "Bearer token",
                "X-Client-Version": oqtant_client.version,
            },
            params={"run": 1},
            timeout=(5, 30),
        )
        assert str(oqtant_job.external_id) == job_payload["external_id"]
        assert isinstance(oqtant_job, OqtantJob)


@patch("oqtant.oqtant_client.requests.get")
def test_get_job_authorization_error(mock_get, settings, mock_response):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_get.return_value = mock_response(None, 401)
    with pytest.raises(api_exceptions.OqtantAuthorizationError):
        oqtant_client.get_job(job_id="test_string")


@patch("oqtant.oqtant_client.requests.get")
def test_get_job_request_error(mock_get, settings, mock_response):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_get.return_value = mock_response(None, 502)
    with pytest.raises(api_exceptions.OqtantRequestError):
        oqtant_client.get_job(job_id="test_string")


@patch("oqtant.oqtant_client.requests.get")
def test_get_job_validation_error(mock_get, settings, mock_response, mock_external_id):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_get.return_value = mock_response({"foo": "bar"}, 200)
    with pytest.raises(api_exceptions.OqtantJobValidationError):
        oqtant_client.get_job(job_id=mock_external_id)


@patch("oqtant.oqtant_client.requests.post")
def test_submit_job(
    mock_post, settings, mock_response, mock_job_create, mock_external_id, job_fixtures
):
    oqtant_client = OqtantClient(settings=settings, token="token")
    jobs = [
        OqtantJob(**job_fixtures.pending_bec_job.model_dump()),
        OqtantJob(**job_fixtures.post_barrier_job.model_dump()),
    ]
    for job in jobs:
        mock_post.return_value = mock_response(mock_job_create, 200)
        result = oqtant_client.submit_job(job=job)
        mock_post.assert_called_with(
            url=oqtant_client.base_url,
            headers={
                "Authorization": "Bearer token",
                "X-Client-Version": oqtant_client.version,
            },
            json={
                "name": job.name,
                "job_type": job.job_type,
                "input_count": len(job.inputs),
                "inputs": [input.model_dump() for input in job.inputs],
            },
            timeout=(5, 30),
        )
        assert result["job_id"] == mock_external_id


@patch("oqtant.oqtant_client.requests.post")
def test_submit_job_with_write(
    mock_post, settings, mock_response, mock_job_create, job_fixtures
):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_post.return_value = mock_response(mock_job_create, 200)
    oqtant_client.write_job_to_file = MagicMock()
    oqtant_client.submit_job(
        job=OqtantJob(**job_fixtures.pending_bec_job.model_dump()), write=True
    )
    assert oqtant_client.write_job_to_file.call_count == 1


def test_submit_job_validation_error(settings):
    oqtant_client = OqtantClient(settings=settings, token="token")
    with pytest.raises(api_exceptions.OqtantJobValidationError):
        oqtant_client.submit_job(job={"foo": "bar"})


@patch("oqtant.oqtant_client.requests.post")
def test_submit_job_authorization_error(
    mock_post, settings, job_fixtures, mock_response
):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_post.return_value = mock_response(None, 401)
    with pytest.raises(api_exceptions.OqtantAuthorizationError):
        oqtant_client.submit_job(
            job=OqtantJob(**job_fixtures.pending_bec_job.model_dump())
        )


@patch("oqtant.oqtant_client.requests.post")
def test_submit_job_request_error(mock_post, settings, job_fixtures, mock_response):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_post.return_value = mock_response(None, 502)
    with pytest.raises(api_exceptions.OqtantRequestError):
        oqtant_client.submit_job(
            job=OqtantJob(**job_fixtures.pending_bec_job.model_dump())
        )


@patch("oqtant.oqtant_client.requests.put")
def test_cancel_job(mock_put, settings, mock_response, mock_auth0_external_id):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_put.return_value = mock_response(None, 200)
    result = oqtant_client.cancel_job(job_id=mock_auth0_external_id)
    assert not result


@patch("oqtant.oqtant_client.requests.put")
def test_cancel_job_request_exception(
    mock_put, settings, mock_response, mock_auth0_external_id
):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_put.return_value = mock_response(None, 502)
    with pytest.raises(api_exceptions.OqtantRequestError):
        oqtant_client.cancel_job(job_id=mock_auth0_external_id)


@patch("oqtant.oqtant_client.requests.put")
def test_cancel_job_authorization_error(
    mock_put, settings, mock_response, mock_auth0_external_id
):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_put.return_value = mock_response(None, 403)
    with pytest.raises(api_exceptions.OqtantAuthorizationError):
        oqtant_client.cancel_job(job_id=mock_auth0_external_id)


@patch("oqtant.oqtant_client.requests.delete")
def test_delete_job(mock_put, settings, mock_response, mock_auth0_external_id):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_put.return_value = mock_response(None, 200)
    result = oqtant_client.delete_job(job_id=mock_auth0_external_id)
    assert not result


@patch("oqtant.oqtant_client.requests.delete")
def test_delete_job_request_exception(
    mock_put, settings, mock_response, mock_auth0_external_id
):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_put.return_value = mock_response(None, 502)
    with pytest.raises(api_exceptions.OqtantRequestError):
        oqtant_client.delete_job(job_id=mock_auth0_external_id)


@patch("oqtant.oqtant_client.requests.delete")
def test_delete_job_authorization_error(
    mock_put, settings, mock_response, mock_auth0_external_id
):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_put.return_value = mock_response(None, 403)
    with pytest.raises(api_exceptions.OqtantAuthorizationError):
        oqtant_client.delete_job(job_id=mock_auth0_external_id)


def test_run_jobs(settings, mock_job_create, mock_external_id, job_fixtures):
    oqtant_client = OqtantClient(settings=settings, token="token")
    oqtant_client.submit_job = MagicMock()
    oqtant_client.submit_job.return_value = mock_job_create
    jobs = [
        job_fixtures.pending_bec_job,
        job_fixtures.post_barrier_job,
        job_fixtures.post_paint1d_job,
    ]
    submitted_jobs = oqtant_client.run_jobs(
        job_list=[OqtantJob(**job.model_dump()) for job in jobs]
    )
    assert submitted_jobs == [mock_external_id] * len(submitted_jobs)


@patch("oqtant.oqtant_client.requests.post")
def test_run_jobs_with_write(
    mock_post, settings, mock_response, mock_job_create, job_fixtures
):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_post.return_value = mock_response(mock_job_create, 204)
    oqtant_client.write_job_to_file = MagicMock()
    oqtant_client.run_jobs(
        job_list=[OqtantJob(**job_fixtures.pending_bec_job.model_dump())], write=True
    )
    assert oqtant_client.write_job_to_file.call_count == 1


@patch("oqtant.oqtant_client.requests.post")
def test_run_jobs_list_limit_error(
    mock_post, settings, mock_response, mock_job_create, job_fixtures
):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_post.return_value = mock_response(mock_job_create, 204)
    with pytest.raises(api_exceptions.OqtantJobListLimitError):
        oqtant_client.run_jobs(
            job_list=[OqtantJob(**job_fixtures.pending_bec_job.model_dump())]
            * (settings.run_list_limit + 1)
        )


@patch("oqtant.oqtant_client.requests.get")
def test_track_jobs(mock_get, settings, mock_response, mock_external_id, job_fixtures):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_get.side_effect = [
        mock_response(job_fixtures.running_bec_job.model_dump(), 200),
        mock_response(job_fixtures.complete_bec_job.model_dump(), 200),
    ]
    oqtant_client.track_jobs(
        pending_jobs=[OqtantJob(**job_fixtures.pending_bec_job.model_dump())]
    )
    assert mock_get.call_count == 2


@patch("oqtant.oqtant_client.requests.get")
def test_track_jobs_with_write(
    mock_get, settings, mock_response, mock_external_id, job_fixtures
):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_get.side_effect = [
        mock_response(job_fixtures.running_bec_job.model_dump(), 200),
        mock_response(job_fixtures.complete_bec_job.model_dump(), 200),
    ]
    oqtant_client.write_job_to_file = MagicMock()
    oqtant_client.track_jobs(
        pending_jobs=[OqtantJob(**job_fixtures.pending_bec_job.model_dump())],
        write=True,
    )
    assert oqtant_client.write_job_to_file.call_count == 1


@patch("oqtant.oqtant_client.requests.get")
@patch("oqtant.oqtant_client.jwt.decode")
def test_get_job_limits(
    mock_decode, mock_get, settings, mock_auth0_external_id, mock_response
):
    expected_job_limits = {
        "quotas": [
            {"quota_period": "1 Month", "quota_limit": 10, "quota_remaining": 5}
        ],
        "rates": [
            {"rate_period": "1 day, 0:00:00", "rate_limit": 10, "rate_remaining": 5}
        ],
    }
    mock_decode.return_value = {"sub": mock_auth0_external_id}
    mock_get.return_value = mock_response(expected_job_limits, 200)
    oqtant_client = OqtantClient(settings=settings, token="token")
    job_limits = oqtant_client.get_job_limits()
    assert job_limits == expected_job_limits


@patch("oqtant.oqtant_client.jwt.decode")
def test_get_job_limits_token_error(mock_decode, settings):
    mock_decode.side_effect = Exception
    oqtant_client = OqtantClient(settings=settings, token="token")
    with pytest.raises(api_exceptions.OqtantTokenError):
        oqtant_client.get_job_limits()


@patch("oqtant.oqtant_client.requests.get")
@patch("oqtant.oqtant_client.jwt.decode")
def test_get_job_limits_authorization_error(
    mock_decode, mock_get, settings, mock_auth0_external_id, mock_response
):
    mock_decode.return_value = {"sub": mock_auth0_external_id}
    mock_get.return_value = mock_response(None, 401)
    oqtant_client = OqtantClient(settings=settings, token="token")
    with pytest.raises(api_exceptions.OqtantAuthorizationError):
        oqtant_client.get_job_limits()


@patch("oqtant.oqtant_client.requests.get")
@patch("oqtant.oqtant_client.jwt.decode")
def test_get_job_limits_request_error(
    mock_decode, mock_get, settings, mock_auth0_external_id, mock_response
):
    mock_decode.return_value = {"sub": mock_auth0_external_id}
    mock_get.return_value = mock_response(None, 502)
    oqtant_client = OqtantClient(settings=settings, token="token")
    with pytest.raises(api_exceptions.OqtantRequestError):
        oqtant_client.get_job_limits()


@patch("oqtant.oqtant_client.requests.get")
def test_search_jobs(mock_get, settings, mock_response):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_get.return_value = mock_response({"items": ["foo", "bar"]}, 200)
    search_results = oqtant_client.search_jobs(job_type=job_schema.JobType.BEC)
    mock_get.assert_called_with(
        url=oqtant_client.base_url,
        headers={
            "Authorization": "Bearer token",
            "X-Client-Version": oqtant_client.version,
        },
        params={"job_type": job_schema.JobType.BEC, "limit": 100},
        timeout=(5, 30),
    )
    assert len(search_results) == 2


@patch("oqtant.oqtant_client.requests.get")
def test_search_jobs_authorization_error(mock_get, settings, mock_response):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_get.return_value = mock_response(None, 401)
    with pytest.raises(api_exceptions.OqtantAuthorizationError):
        oqtant_client.search_jobs(job_type=job_schema.JobType.BEC)


@patch("oqtant.oqtant_client.requests.get")
def test_search_jobs_request_error(mock_get, settings, mock_response):
    oqtant_client = OqtantClient(settings=settings, token="token")
    mock_get.return_value = mock_response(None, 502)
    with pytest.raises(api_exceptions.OqtantRequestError):
        oqtant_client.search_jobs(job_type=job_schema.JobType.BEC)


def test_write_job_to_file_default(settings, job_fixtures):
    oqtant_client = OqtantClient(settings=settings, token="token")
    job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
    with patch("builtins.open", mock_open()) as context:
        oqtant_client.write_job_to_file(job=job)
        context.assert_called_with(f"{job.id}.txt", "w+")
        context().write.assert_called_with(str(job.model_dump_json()))


def test_write_job_to_file_custom_name(settings, job_fixtures):
    oqtant_client = OqtantClient(settings=settings, token="token")
    job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
    with patch("builtins.open", mock_open()) as context:
        oqtant_client.write_job_to_file(job=job, file_name="foo")
        context.assert_called_with("foo.txt", "w+")
        context().write.assert_called_with(str(job.model_dump_json()))


def test_write_job_to_file_custom_path(settings, job_fixtures):
    oqtant_client = OqtantClient(settings=settings, token="token")
    job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
    with patch("builtins.open", mock_open()) as context:
        oqtant_client.write_job_to_file(job=job, file_path="~/foo/bar.txt")
        context.assert_called_with("~/foo/bar.txt", "w+")
        context().write.assert_called_with(str(job.model_dump_json()))


def test_write_job_to_file_exception(settings, job_fixtures):
    oqtant_client = OqtantClient(settings=settings, token="token")
    job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
    with patch("builtins.open", mock_open()) as context:
        context().write.side_effect = Exception
        with pytest.raises(api_exceptions.JobWriteError):
            oqtant_client.write_job_to_file(job=job, file_path="~/foo/bar.txt")
            context.assert_called_with("~/foo/bar.txt", "w+")
            context().write.assert_called_with(str(job.model_dump_json()))


def test_load_job_from_file_with_refresh(settings, job_fixtures):
    oqtant_client = OqtantClient(settings=settings, token="token")
    job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
    oqtant_client.write_job_to_file = MagicMock()
    oqtant_client.get_job = MagicMock()
    oqtant_client.get_job.return_value = job
    with patch(
        "builtins.open", mock_open(read_data=str(job.model_dump_json()))
    ) as context:
        loaded_job = oqtant_client.load_job_from_file(file_path="~/foo/bar.txt")
        context.assert_called_with("~/foo/bar.txt")
        oqtant_client.get_job.assert_called_with(job.external_id, run=1)
        oqtant_client.write_job_to_file.assert_called_with(
            job, file_path="~/foo/bar.txt"
        )
        assert loaded_job.name == job.name


def test_load_job_from_file_with_no_refresh(settings, job_fixtures):
    oqtant_client = OqtantClient(settings=settings, token="token")
    job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
    with patch(
        "builtins.open", mock_open(read_data=str(job.model_dump_json()))
    ) as context:
        loaded_job = oqtant_client.load_job_from_file(
            file_path="~/foo/bar.txt", refresh=False
        )
        context.assert_called_with("~/foo/bar.txt")
        assert loaded_job.name == job.name


def test_load_job_from_file_not_found_error(settings, job_fixtures):
    oqtant_client = OqtantClient(settings=settings, token="token")
    with patch("builtins.open", mock_open()) as context:
        context.side_effect = FileNotFoundError
        with pytest.raises(api_exceptions.JobReadError):
            oqtant_client.load_job_from_file(file_path="~/foo/bar.txt")
            context.assert_called_with("~/foo/bar.txt")


def test_load_job_from_file_validation_error(settings, job_fixtures):
    oqtant_client = OqtantClient(settings=settings, token="token")
    job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
    oqtant_client.get_job = MagicMock()
    oqtant_client.get_job.side_effect = KeyError
    with patch(
        "builtins.open", mock_open(read_data=str(job.model_dump_json()))
    ) as context:
        with pytest.raises(api_exceptions.JobReadError):
            oqtant_client.load_job_from_file(file_path="~/foo/bar.txt")
            context.assert_called_with("~/foo/bar.txt")
            oqtant_client.get_job.assert_called_with(job.external_id)


def test_load_job_from_file_request_error(settings, job_fixtures):
    oqtant_client = OqtantClient(settings=settings, token="token")
    job = OqtantJob(**job_fixtures.pending_bec_job.model_dump())
    oqtant_client.get_job = MagicMock()
    oqtant_client.get_job.side_effect = RequestException
    with patch(
        "builtins.open", mock_open(read_data=str(job.model_dump_json()))
    ) as context:
        with pytest.raises(api_exceptions.JobReadError):
            oqtant_client.load_job_from_file(file_path="~/foo/bar.txt")
            context.assert_called_with("~/foo/bar.txt")
            oqtant_client.get_job.assert_called_with(job.external_id, run=1)
