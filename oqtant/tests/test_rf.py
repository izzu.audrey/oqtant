import pytest

from oqtant.schemas.rf import *


def test_rf_evap_from_input():
    js_rf_evaporation = job_schema.RfEvaporation(
        times_ms=[80, -2, 3, -4, 5],
        frequencies_mhz=[2, 3, 5, 8, 1],
        powers_mw=[3, 2, 4, 5, 3],
        interpolation="LINEAR",
    )
    rf = RfEvap.from_input(js_rf_evaporation)
    assert rf.times_ms == [-2.0, 0.0]
    assert rf.frequencies_mhz == [8.0, 3.0]
    assert rf.powers_mw == [5.0, 2.0]
