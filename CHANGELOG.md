## v0.15.0 (2023-05-31)

### added (2 changes)

- [Batch Job Access Disclaimers](coldquanta/albert/pybert@639061c6979a3e2738bb42ea89c30301c3171407) ([merge request](coldquanta/albert/pybert!49))
- [Automatic Changelog](coldquanta/albert/pybert@3c1a659fc510cafde5a0388fc737b1c598b90239) ([merge request](coldquanta/albert/pybert!48))

## v0.16.0 (2023-07-12)

### added (1 change)

- [ALB-4322: Optional Auth Server Port](coldquanta/albert/pybert@2811767c238ce3aeae80276c87c753560a2097e4) ([merge request](coldquanta/albert/pybert!56))

### updated (1 change)

- [Conda Instructions, Cleanup Terminology](coldquanta/albert/pybert@02e802f7676754228326e93d34719b9368202d57) ([merge request](coldquanta/albert/pybert!55))

## v0.16.1 (2023-07-12)

### updated (2 changes)

- [improved server port instructions, version bump](coldquanta/albert/pybert@8de2498dd51fdb914324e53b6b8d79f988b5a153)
- [Walkthrough Fixes and Updates](coldquanta/albert/pybert@409f08f6bc80bab4c81972a5fb13f99717bad1e8) ([merge request](coldquanta/albert/pybert!54))

## v0.16.2 (2023-07-24)

### fixed (1 change)

- [Remove unused statement from cell](coldquanta/albert/pybert@b2863559f812cebf2395d450fc93cdc99db3427c)

### added (1 change)

- [ALB-4193: Copyrights](coldquanta/albert/pybert@2890f22d0e93b04464761723739fdf20b282012a) ([merge request](coldquanta/albert/pybert!58))

## v0.16.3 (2023-08-10)

### updated (1 change)

- [ALB-4632: New Domain && Audiences](coldquanta/albert/pybert@05b0ec1e095082fd6f6662573f4a6041b054013a) ([merge request](coldquanta/albert/pybert!61))

## v1.0.0 (2023-09-15)

### updated (6 changes)

- [ALB-4673: Production Settings](coldquanta/albert/pybert@02c9e67b0ffb5f6ca1db63af76915d4bc7ef1170) ([merge request](coldquanta/albert/pybert!67))
- [ALB-4274: Conversion Simplified job creation with schema changes. Improved...](coldquanta/albert/pybert@a312802056cdc9b5571c69d528411b552b1318ba) ([merge request](coldquanta/albert/pybert!63))
- [Oqtant Review Adjustments](coldquanta/albert/pybert@b243271fab4b2aca909eee5417eb1e43c72c084e) ([merge request](coldquanta/albert/pybert!60))
- [Changes 2D plot origins to align with ICS, and hence gravity.](coldquanta/albert/pybert@688881141cd82928172bd55c8e426455b216fe0c) ([merge request](coldquanta/albert/pybert!68))

### removed (1 change)

- [Removes 'demos' folder for now, at least until they are more fully developed.](coldquanta/albert/pybert@f93923736eaa05cc52be1d3f1ac349e468f6a896) ([merge request](coldquanta/albert/pybert!64))

## v1.0.1 (2023-09-25)

### added (2 changes)

- [ALB-4889: Limit Info On Client Create](coldquanta/albert/pybert@00a88874d0f4331462f957bec6e52bbc163be6b1) ([merge request](coldquanta/albert/pybert!66))
- [ALB-4737: Pixcal](coldquanta/albert/pybert@d0dd104611da927e12fff748786892f16748ffd9) ([merge request](coldquanta/albert/pybert!65))

### updated (1 change)

- [oqtant name updates](coldquanta/albert/pybert@067c7966f772de26651a852b1edcdea789f496cc) ([merge request](coldquanta/albert/pybert!70))

## v1.4.0 (2023-11-02)

### Note: version 1.4.0 of Oqtant (OqtAPI) contains structural/workflow changes to user authentication, submission, and result retrieval. Please reference updated walkthroughs for working examples.

### updated (7 changes)

- [walkthrough text and reorganization](coldquanta/albert/pybert@dd1f8d81f0c42accd629adcc576325f09f4dc52e) ([merge request](coldquanta/albert/pybert!94))
- [Refactor to simplify usage. Removes direct client use.](coldquanta/albert/pybert@d5b7b92c610175a4cb45a168a4dfe99aa0b8bb69) ([merge request](coldquanta/albert/pybert!93))
- [ALB-5375: Update Doc Links](coldquanta/albert/pybert@7f0ad10996f958bc76c887239ad578c66ec2aeb7) ([merge request](coldquanta/albert/pybert!90))
- [Walkthrough edits from Victor](coldquanta/albert/pybert@1dff3332a5f9d2d151f9f45050c948d99d61a9ac) ([merge request](coldquanta/albert/pybert!75))
- [Updates validation for QuantumMatter object inputs.](coldquanta/albert/pybert@b00c3859ef236f63dde7f9199b99d7e981c73b44) ([merge request](coldquanta/albert/pybert!81))
- [Alters default RF evaporation values, when no temperature is given, to match...](coldquanta/albert/pybert@dc5f913acc10e4a56acae40bd7863ee275054f6b) ([merge request](coldquanta/albert/pybert!88))
- [Further refines relationship between user's desired temperature and final RF knife frequency.](coldquanta/albert/pybert@9049866bcc67da8649bd1f4678bffcba6628d5a4) ([merge request](coldquanta/albert/pybert!84))

### added (3 change)

- [ALB-4079 adding changelog process to README-DEV](coldquanta/albert/pybert@cbfa5b699d824f5940defadca4aacb99ab27ec72) ([merge request](coldquanta/albert/pybert!89))
- [Adds basic demo notebook for exploring quantum interference.](coldquanta/albert/pybert@cd85fbc8ee5ab91699f1e0974039d665291aeffa) ([merge request](coldquanta/albert/pybert!83))
- [QuantumMatterFactory abstraction layer for uniform, one-stop shopping for creating schema objects.](coldquanta/albert/pybert@8f78ffde77b2b52d93ed304a368b90608e15bea0) ([merge request](coldquanta/albert/pybert!74))

### fixed (8 changes)

- [Fixes minor bug in interference demo.](coldquanta/albert/pybert@730cd26f8751763302043433c8483596f3695f6e) ([merge request](coldquanta/albert/pybert!85))
- [Fixup of walkthrough 5.](coldquanta/albert/pybert@bdf9ee37d8b9c24b74bfb93e7545e6a22b5c8ee2) ([merge request](coldquanta/albert/pybert!76))
- [Realistic optical potentials](coldquanta/albert/pybert@ebd296087b0ba3e974f337b1a073c9ab7ce1ec76) ([merge request](coldquanta/albert/pybert!78))
- [re-calibration of relationship between last rf evaporation frequency and cloud temperature](coldquanta/albert/pybert@d168d8e50fe175c377f57afa5bd464fdc4b4109e) ([merge request](coldquanta/albert/pybert!80))
- [ALB-5271 input count](coldquanta/albert/pybert@e0592be3886b20d6f03c88bcf5701917cc14eb65) ([merge request](coldquanta/albert/pybert!82))
- [ALB-5150 fixed links, names in readme.md.](coldquanta/albert/pybert@ff74cfd3b4233e3dc882cc3ac1233238183c218f) ([merge request](coldquanta/albert/pybert!79))
- [barrier from input](coldquanta/albert/pybert@119473b8b272e06a3310711fde5d19859129f487) ([merge request](coldquanta/albert/pybert!77))
- [Removes html copies of walkthroughs since they are always out of date.](coldquanta/albert/pybert@e7fb76180d6edf806058d534d1259ec7e913cebe) ([merge request](coldquanta/albert/pybert!86))

## v1.6.0 (2024-01-08)

### removed (1 change)

- [ALB-6044: Remove Queue Status From Client Submit](coldquanta/albert/pybert@ecadcc9b2c1347114ada8a82f8e653b27d8e4a40) ([merge request](coldquanta/albert/pybert!107))

### updated (2 changes)

- [ALB-5981: Link & Text Updates](coldquanta/albert/pybert@de43661df2ef4442e8ddc1d94264f20007364427) ([merge request](coldquanta/albert/pybert!106))
- [ALB-5930: Updated README Links](coldquanta/albert/pybert@2657676dd4cacb5b3cdfbc6031d0200ac8a3a944) ([merge request](coldquanta/albert/pybert!103))

### added (5 changes)

- [Oqtant docstrings](coldquanta/albert/pybert@c6f82d075d992a3770a142c8cf62f954ff13f559) ([merge request](coldquanta/albert/pybert!101))
- [added image type to creation of qm object in from_input so that qm objects...](coldquanta/albert/pybert@bd6239b166969364b704b7585149460f55b27de4) ([merge request](coldquanta/albert/pybert!104))
- [Commas in hello world](coldquanta/albert/pybert@b59c445f4e005c3dbf91cf917eb9c644724b3684) ([merge request](coldquanta/albert/pybert!105))
- [Pre-pre release](coldquanta/albert/pybert@8c1d6043fcc0da0282012c3bcb524170702f0272) ([merge request](coldquanta/albert/pybert!100))
- [Defaults](coldquanta/albert/pybert@018c70e190e9f124a351dc475b53633335cb94a2) ([merge request](coldquanta/albert/pybert!95))

### fixed (3 change)

- [Fixing spelling in title](coldquanta/albert/pybert@2014015d99e4f8a804185f0f14866a40605f884f) ([merge request](coldquanta/albert/pybert!102))
- [File save load fix](coldquanta/albert/pybert@6585a6df3f1387a4c509374e4772b31037be6534) ([merge request](coldquanta/albert/pybert!98))
- [typo and link fixes for user facing docs, demo 1](coldquanta/albert/pybert@fe038911ae19868fb39a4d36b6168745fe5f06d8) ([merge request](coldquanta/albert/pybert!97))

### added (1 change)

- [ALB-5624: Cancel Jobs](coldquanta/albert/pybert@c7f4267003126a28b9babd6c6df3028f0708134b) ([merge request](coldquanta/albert/pybert!96))
## v1.6.1 (2024-01-12)

### updated (1 change)

- [Barrier min handling](coldquanta/albert/pybert@4868c460e3409bcf81eed36fe6c634e1bf128f44) ([merge request](coldquanta/albert/pybert!108))

## v.1.7.1 (2024-01-23)

### fixed (2 change)

- [ALB-6197: Hello World](coldquanta/albert/pybert@82e22200be20fd224d3e9ce52fc56cf231ac6ee7) ([merge request](coldquanta/albert/pybert!116))
- [Fix landscape visualizer](coldquanta/albert/pybert@e4ddb8bf5a140828aab1e360056e3c011749ea9c) ([merge request](coldquanta/albert/pybert!112))

### removed (1 change)

- [remove sim demo notebook for now](coldquanta/albert/pybert@1d5c0b9d1fb753cdfc6ce56670c14ae977a2aaff) ([merge request](coldquanta/albert/pybert!114))

### added (1 change)

- [ALB-6086: Job Start && Complete Times](coldquanta/albert/pybert@c9410f03aab1ace97159ac1fa6a6cad9c19eb43d) ([merge request](coldquanta/albert/pybert!111))

### updated (2 changes)

- [Barrier object language](coldquanta/albert/pybert@316aa0dd807c204ac0b5e02481dfdf10e7c73117) ([merge request](coldquanta/albert/pybert!110))

## v1.8.0 (2024-01-29)

### updated (2 change)

- [simulator review for release: TOF, animations, and walkthrough](coldquanta/albert/pybert@36daaa78cbbd3976975ac3ca78afefc6ad58802e) ([merge request](coldquanta/albert/pybert!118))
- [Oqtant schema consolidation](coldquanta/albert/pybert@64eb03f5e5f96bbdbbc430627390bef7d3fb8fe7) ([merge request](coldquanta/albert/pybert!113))

### added (1 change)

- [Copyrights](coldquanta/albert/pybert@6c511c4f186ae3f53da14d0489d8e9d58e65da17) ([merge request](coldquanta/albert/pybert!120))

## v1.8.1 (2024-01-29)

### fixed (1 change)

- [corrected get_result call to get oqtant output](coldquanta/albert/pybert@b42a95291c1c3c168a894a84dd346d56ed323188) ([merge request](coldquanta/albert/pybert!121))
## v1.8.2 (2024-01-30)

### fixed (1 change)

- [fix plot_tof](coldquanta/albert/pybert@652609fcc056f2a35c8c8ddd7cda77cb1a069f68)

### added (1 changes)

- [Demo 2 complete](coldquanta/albert/pybert@c7ce0c575d63548e6d87055dba64191650058d5f) ([merge request](coldquanta/albert/pybert!122))

### updated (1 change)

- [Demo 2 small update](coldquanta/albert/pybert@648612dbc5f3abed8678aeda1d869c63424ee664) ([merge request](coldquanta/albert/pybert!123))
## v1.9.0 (2024-02-07)

### removed (1 change)

- [Projected Changes](coldquanta/albert/pybert@767bd7f7abf152035b9d64ceb54c06e7bca3b601) ([merge request](coldquanta/albert/pybert!127))

### added (1 change)

- [Adding a README to the Simulator](coldquanta/albert/pybert@608595beec48e77cb428b6256813722c63377a12) ([merge request](coldquanta/albert/pybert!126))

### added|removed|updated|fixed (2 changes)

- [corrected typo in user fitting method TF_dist_2D](coldquanta/albert/pybert@27536287ccef402c4aaa5bdfa21f4b7665e51d25) ([merge request](coldquanta/albert/pybert!125))
- [removed html from walkthrough 6](coldquanta/albert/pybert@ae5aca13389a7232187d130d77c0eec23d8ef593) ([merge request](coldquanta/albert/pybert!124))

### updated (2 changes)

- [ALB-6168: Load Failed Jobs](coldquanta/albert/pybert@6b75f28f979fa977b308e03a42170cb32b46563b) ([merge request](coldquanta/albert/pybert!117))
- [ALB-5926: Batch Input Identification](coldquanta/albert/pybert@962098bf439e2915b08b036ee7423b10e13ee5e5) ([merge request](coldquanta/albert/pybert!119))
## v1.10.0 (2024-02-16)

### added|removed|updated|fixed (1 change)

- [Terminator abstraction](coldquanta/albert/pybert@fd18bdc0722b65fb170bf059a661cc72772a60d2) ([merge request](coldquanta/albert/pybert!128))

### updated (1 change)

- [doc links](coldquanta/albert/pybert@aa31a28b24a8e9031fe4927ac766aa535a9d6d27) ([merge request](coldquanta/albert/pybert!129))
## v1.10.2 (2024-02-20)

No changes.
## v1.11.1 (2024-03-04)

### updated (2 changes)

- [ALB-6376: Batch Jobs Results](coldquanta/albert/pybert@9c044fa8e32f51cc904c8ba3cf2a27d9fecd1efb) ([merge request](coldquanta/albert/pybert!134))
- [ALB-6369: Use Access Token File](coldquanta/albert/pybert@892a272cef27ce97130dd015eaa5c64a7cee18c9) ([merge request](coldquanta/albert/pybert!132))

### added|removed|updated|fixed (1 change)

- [simulator bugs:  added get_grids function, time tickers for animations, time...](coldquanta/albert/pybert@7a8a87a47f50a89eaabf3b51011cb05a9367c8b1) ([merge request](coldquanta/albert/pybert!133))

### added (1 change)

- [Notebook starter](coldquanta/albert/pybert@0a7d607e8964699e4291ba865fea584546a93354) ([merge request](coldquanta/albert/pybert!135))

### fixed (1 change)

- [fix doc links](coldquanta/albert/pybert@8a2d43eaf912b8c8814d619f4efa15f9885a41a7)

### removed (1 change)

- [Oqtant Docs Move](coldquanta/albert/pybert@b41a104626507aff178580b6a120413d4f8e9483) ([merge request](coldquanta/albert/pybert!131))
## 1.12.1 (2024-04-15)

### fixed (1 change)

- [Fix simulator run.](coldquanta/albert/pybert@acde3dfde680cc05bf2d8fd058cb74d54d5f6975) ([merge request](coldquanta/albert/pybert!139))

## 1.12.0 (2024-04-08)

### fixed (1 change)

- [fix transistor plotting in show_potential](coldquanta/albert/pybert@364a715ca91012455d804935eb95fcd24866f664) ([merge request](coldquanta/albert/pybert!137))

### updated (1 change)

- [changes related to job limit response changes](coldquanta/albert/pybert@34a1c3d7204609f0239a79dc7cea87bd7fa6c2d3) ([merge request](coldquanta/albert/pybert!138))

### added|removed|updated|fixed (1 change)

- [updated links in README to point to doc site](coldquanta/albert/pybert@d077e966613bf05b5de90a6c81f845a4bd0e9074) ([merge request](coldquanta/albert/pybert!136))
